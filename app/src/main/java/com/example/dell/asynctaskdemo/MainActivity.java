package com.example.dell.asynctaskdemo;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    private InputStream openConnection(String urlStr) {
        InputStream is = null;


        try {
            URL url = new URL(urlStr);
            URLConnection Conn = new url.openConnection();
            if (Conn instanceof HttpURLConnection) {
                // cast into the HttpUrlConnection
                HttpURLConnection httpURLConnection = (HttpURLConnection) Conn;
                int response = -1;
                httpURLConnection.connect();
                response = httpURLConnection.getResponseCode();
                if (response == HttpURLConnection.HTTP_OK) {
                    is = httpURLConnection.getInputStream();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return is;
    }
}
